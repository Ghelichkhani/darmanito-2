const myForm = document.getElementById("custom-contact-form");

myForm.addEventListener("submit", function (e) {
  e.preventDefault();

  let name = document.myform.name.value;
  let tell = document.myform.tell.value;

  let errorTextName = document.getElementById("error_text_name");
  let errorTextPhone = document.getElementById("error_text_phone");
  let successText = document.getElementById("success_text");

  if (name === "" && tell === "") {
    errorTextName.innerHTML = "نام و نام خانوادگی نمیتواند خالی باشد";
    errorTextPhone.innerHTML = "ایمیل یا شماره تلفن نمی تواند خالی باشد";
    setTimeout(() => {
      errorTextName.classList.add("d-none");
    }, 3000);

    setTimeout(() => {
      errorTextPhone.classList.add("d-none");
    }, 3000);
    return false;
  } else if (name == null || name == "") {
    errorTextName.innerHTML = "نام و نام خانوادگی نمیتواند خالی باشد";
    setTimeout(() => {
      errorTextName.classList.add("d-none");
    }, 3000);

    return false;
  } else if (name.length < 3) {
    errorTextName.innerHTML = "نام و نام خانوادگی باید بیش از 3 حرف باشد";
    setTimeout(() => {
      errorTextName.classList.add("d-none");
    }, 3000);
    return false;
  }

  else if (tell === '' || tell.length < 6) {
    errorTextPhone.innerHTML = "ایمیل یا شماره تلفن را صحیح وارد کنید";
    setTimeout(() => {
      errorTextPhone.classList.add("d-none");
    }, 3000);
    return false;
  } else {
    successText.innerHTML = "فرم با موفقیت ارسال شد";
    setTimeout(() => {
      successText.classList.add("d-none");
    }, 3000);
    return true;
  }
});
